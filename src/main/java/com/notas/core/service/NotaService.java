package com.notas.core.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.notas.core.converter.Convertidor;
import com.notas.core.entity.Nota;
import com.notas.core.model.MNota;
import com.notas.core.repository.NotaRepositorio;

@Service("servicio")
public class NotaService {
	@Autowired
	@Qualifier("repositorio")
	private NotaRepositorio repositorio;
	
	@Autowired
	@Qualifier("convertidor")
	private Convertidor convertidor;
	
	public boolean crear (Nota nota){
		try {
			repositorio.save(nota);
			return true;
			
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	
	public boolean modificar (Nota nota){
		try {
			repositorio.save(nota);
			return true;
			
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	
	public boolean borrar ( long id){
		try {
			Nota nota = repositorio.findById(id);
			repositorio.delete(nota);
			return true;
			
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
		
			
		}
	public List<MNota> obtener (){
		
		return convertidor.convertidorLista(repositorio.findAll());
	}
	
	public List<MNota> obtenerPorNombreTitulo (String nombre,String titulo){
		return convertidor.convertidorListaFiltro(repositorio.findByNombreAndTitulo(nombre, titulo));
	}

}

package com.notas.core.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.notas.core.entity.Nota;
import com.notas.core.model.MNota;
import com.notas.core.service.NotaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/v1")
@Api("Servicio Rest de Notas")
public class NotaController {
	@Autowired
	@Qualifier("servicio")
	NotaService service;
	@ApiOperation("Metodo para insercion de notas")
	@PutMapping("/nota")
	public boolean agregarNota(@RequestBody @Valid Nota nota){
		return service.crear(nota);
	}
	@ApiOperation("Metodo para modificacion de notas")
	@PostMapping("/nota")
	public boolean actualizarNota(@RequestBody @Valid Nota nota){
		return service.modificar(nota);
	}
	@ApiOperation("Metodo para obtener notas por ID")
	@GetMapping("/nota")
	public List<MNota> obtenerNota(){
		return service.obtener();
	}
	
	@ApiOperation("Metodo para obtner notas filtrando por nombre y titulo")
	@GetMapping("/notaFiltro/{nombre}/{titulo}")
	public List<MNota> obtenerNotaFiltro(@PathVariable ("nombre") String nombre,@PathVariable ("titulo") String titulo){
		return service.obtenerPorNombreTitulo(nombre, titulo);
	}
	
	@ApiOperation("Metodo eliminar notas por ID")
	@DeleteMapping("/deleteNota")
	public void borrarNota(long id){
		service.borrar(id);
	}
	
}
